// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <sstream>  // istringstream
#include <string>   // getline, string

#include "Allocator.hpp"

// ----
// main
// ----

int main () {
    using namespace std;
    /*
    your code for the read eval print loop (REPL) goes here
    in this project, the unit tests will only be testing Allocator.hpp, not the REPL
    the acceptance tests will be testing the REPL
    */
    int tests;
    cin >> tests;
    string s;
    getline(cin, s); // eat blank line
    getline(cin, s); // eat blank line
    for (int t = 0; t < tests; ++t) {
        my_allocator<double, 1000> a;
        while (getline(cin, s) && s.length()) {
            istringstream rin(s);
            int request;
            rin >> request;
            if (request >= 0) {
                a.allocate(request);
            } else {
                my_allocator<double, 1000>::iterator b = a.begin();
                my_allocator<double, 1000>::iterator e = a.end();
                while (b != e && request < 0) {
                    if (*b < 0) {
                        ++request;
                    }
                    ++b;
                }
                --b; // go back to get the busy block that we actually need to deallocate
                double* start_ptr = reinterpret_cast<double*>(reinterpret_cast<char*>(&(*b)) + sizeof(int));
                a.deallocate(start_ptr, 0);
            }
        }
        my_allocator<double, 1000>::iterator b = a.begin();
        my_allocator<double, 1000>::iterator e = a.end();
        while (b != e) {
            cout << *b;
            if (++b != e) {
                cout << " ";
            }
        }
        cout << endl;
    }
    return 0;
}
