// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.hpp"

TEST(AllocatorFixture, test0) {
    using allocator_type = std::allocator<int>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

TEST(AllocatorFixture, test1) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// operator []
TEST(AllocatorFixture, test2) {
    using allocator_type = my_allocator<int, 1000>;

    allocator_type x;
    ASSERT_EQ(x[0], 992);
    ASSERT_EQ(x[996], 992);
}

// operator [] const
TEST(AllocatorFixture, test3) {
    using allocator_type = my_allocator<int, 1000>;

    const allocator_type x;
    ASSERT_EQ(x[0], 992);
    ASSERT_EQ(x[996], 992);
}

// begin + end + iterator::operator == + iterator::operator !=
TEST(AllocatorFixture, test4) {
    using allocator_type = my_allocator<int, 1000>;
    using iterator_type  = allocator_type::iterator;

    allocator_type x;
    iterator_type b = x.begin();
    iterator_type e = x.end();
    ASSERT_FALSE(b == e);
    ASSERT_TRUE(b != e);
}

// CONST begin + end + iterator::operator == + iterator::operator !=
TEST(AllocatorFixture, test5) {
    using allocator_type = my_allocator<int, 1000>;
    using iterator_type  = allocator_type::const_iterator;

    const allocator_type x;
    iterator_type b = x.begin();
    iterator_type e = x.end();
    ASSERT_FALSE(b == e);
    ASSERT_TRUE(b != e);
}

// iterator::operator *
TEST(AllocatorFixture, test6) {
    using allocator_type = my_allocator<int, 1000>;
    using iterator_type  = allocator_type::iterator;

    allocator_type x;
    iterator_type b = x.begin();
    ASSERT_EQ(*b, 992);
}

// CONST iterator::operator *
TEST(AllocatorFixture, test7) {
    using allocator_type = my_allocator<int, 1000>;
    using iterator_type  = allocator_type::const_iterator;

    const allocator_type x;
    iterator_type b = x.begin();
    ASSERT_EQ(*b, 992);
}

// iterator::operator ++
TEST(AllocatorFixture, test8) {
    using allocator_type = my_allocator<int, 1000>;
    using iterator_type  = allocator_type::iterator;

    allocator_type x;
    iterator_type b1 = x.begin();
    ++b1;
    iterator_type b2 = x.begin();
    b2++;
    iterator_type e = x.end();
    ASSERT_EQ(b1, e);
    ASSERT_EQ(b2, e);
}

// CONST iterator::operator ++
TEST(AllocatorFixture, test9) {
    using allocator_type = my_allocator<int, 1000>;
    using iterator_type  = allocator_type::const_iterator;

    const allocator_type x;
    iterator_type b1 = x.begin();
    ++b1;
    iterator_type b2 = x.begin();
    b2++;
    iterator_type e = x.end();
    ASSERT_EQ(b1, e);
    ASSERT_EQ(b2, e);
}

// iterator::operator --
TEST(AllocatorFixture, test10) {
    using allocator_type = my_allocator<int, 1000>;
    using iterator_type  = allocator_type::iterator;

    allocator_type x;
    iterator_type e1 = x.end();
    --e1;
    iterator_type e2 = x.end();
    e2--;
    iterator_type b = x.begin();
    ASSERT_EQ(e1, b);
    ASSERT_EQ(e2, b);
}

// CONST iterator::operator --
TEST(AllocatorFixture, test11) {
    using allocator_type = my_allocator<int, 1000>;
    using iterator_type  = allocator_type::const_iterator;

    const allocator_type x;
    iterator_type e1 = x.end();
    --e1;
    iterator_type e2 = x.end();
    e2--;
    iterator_type b = x.begin();
    ASSERT_EQ(e1, b);
    ASSERT_EQ(e2, b);
}

// my_allocator () exception
TEST(AllocatorFixture, test12) {
    using allocator_type = my_allocator<int, 4>;

    bool result = false;
    try {
        allocator_type x;
    }
    catch (std::bad_alloc& e) {
        result = true;
    }
    ASSERT_TRUE(result);
}

// allocate () exception
TEST(AllocatorFixture, test13) {
    using allocator_type = my_allocator<int, 1000>;

    int result = 0;
    allocator_type x;
    try {
        x.allocate(0);
    }
    catch (std::bad_alloc& e) {
        ++result;
    }

    try {
        x.allocate(250);
    }
    catch (std::bad_alloc& e) {
        ++result;
    }
    ASSERT_EQ(result, 2);
}

// allocate block normal block + entire block
TEST(AllocatorFixture, test14) {
    using allocator_type = my_allocator<int, 1000>;
    using iterator_type  = allocator_type::iterator;

    allocator_type x;
    x.allocate(10);
    x.allocate(236);
    iterator_type it = x.begin();
    ASSERT_EQ(*it, -40);
    ++it;
    ASSERT_EQ(*it, -944);
}

// deallocate () exceptions
TEST(AllocatorFixture, test15) {
    using allocator_type = my_allocator<int, 1000>;

    int result = 0;
    allocator_type x;
    try {
        x.deallocate(&x[0] - 1, 0);
    } catch (std::invalid_argument& e) {
        ++result;
    }

    try {
        x.deallocate(&x[0] + 1, 0);
    } catch (std::invalid_argument& e) {
        ++result;
    }
    ASSERT_EQ(result, 2);
}

// coalesce left
TEST(AllocatorFixture, test16) {
    using allocator_type = my_allocator<int, 1000>;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    pointer p1 = x.allocate(1);
    pointer p2 = x.allocate(1);
    x.allocate(1);
    x.deallocate(p1, 0);
    x.deallocate(p2, 0);
    ASSERT_EQ(x[0], 16);
}

// coalesce right
TEST(AllocatorFixture, test17) {
    using allocator_type = my_allocator<int, 1000>;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    pointer p = x.allocate(1);
    x.deallocate(p, 0);
    ASSERT_EQ(x[0], 992);
}

// coalesce both sides
TEST(AllocatorFixture, test18) {
    using allocator_type = my_allocator<int, 1000>;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    pointer p1 = x.allocate(1);
    pointer p2 = x.allocate(1);
    x.deallocate(p1, 0);
    x.deallocate(p2, 0);
    ASSERT_EQ(x[0], 992);
}

TEST(AllocatorFixture, test19) {
    using allocator_type = my_allocator<int, 1000>;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    pointer p = x.allocate(1);
    x.construct(p, 1337);
    ASSERT_EQ(*p, 1337);
    x.destroy(p);
    x.deallocate(p, 1);
}
