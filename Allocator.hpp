// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>      // assert
#include <cmath>        // abs
#include <cstddef>      // ptrdiff_t, size_t
#include <new>          // bad_alloc, new
#include <stdexcept>    // invalid_argument

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return lhs._p == rhs._p;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        int& operator * () const {
            return *reinterpret_cast<int*>(_p);
        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
            int block_size = abs(*reinterpret_cast<int*>(_p));
            char* next_p = reinterpret_cast<char*>(_p) + block_size + sizeof(int) * 2;
            _p = reinterpret_cast<int*>(next_p);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        iterator& operator -- () {
            int block_size = abs(*reinterpret_cast<int*>(_p - 1));
            char* next_p = reinterpret_cast<char*>(_p) - block_size - sizeof(int) * 2;
            _p = reinterpret_cast<int*>(next_p);
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return lhs._p == rhs._p;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const {
            return *reinterpret_cast<const int*>(_p);
        }

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () {
            int block_size = abs(*reinterpret_cast<const int*>(_p));
            const char* next_p = reinterpret_cast<const char*>(_p) + block_size + sizeof(int) * 2;
            _p = reinterpret_cast<const int*>(next_p);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {
            int block_size = abs(*reinterpret_cast<const int*>(_p - 1));
            const char* next_p = reinterpret_cast<const char*>(_p) - block_size - sizeof(int) * 2;
            _p = reinterpret_cast<const int*>(next_p);
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * ensures the integrity of the heap array
     * returns false if sentinels are different or 2 free blocks are adjacent
     */
    bool valid () const {
        const_iterator b = begin();
        ++b;
        while (b != end()) {
            int block_size = abs(*b);
            const int* end_ptr = reinterpret_cast<const int*>(reinterpret_cast<const char*>(&(*b)) + block_size + sizeof(int));
            if (*b != *end_ptr) {
                // check if sentinels are different
                return false;
            }

            if (b != begin()) {
                const_iterator prev = --const_iterator(&(*b));
                if (*prev > 0 && *b > 0) {
                    // check for adjacent free blocks
                    return false;
                }
            }

            ++b;
        }
        return true;
    }

    // --------
    // coalesce
    // --------

    /**
    * O(1) in space
    * O(1) in time
    * merge adjacent free blocks together into a single free block
    */
    void coalesce (int* p) {
        iterator it = iterator(p);
        iterator prev = --iterator(p);
        bool prev_valid = &(*prev) >= &(*this)[0]; // prev is not out of bounds
        iterator next = ++iterator(p);
        bool next_valid = &(*next) < &(*this)[N];  // next is not out of bounds
        if (prev_valid && *prev > 0 && next_valid && *next > 0) { // coalesce both sides
            int new_block_size = *prev + *it + *next + sizeof(int) * 4;
            int* end_ptr = reinterpret_cast<int*>(reinterpret_cast<char*>(&(*prev)) + new_block_size + sizeof(int));
            *prev = new_block_size;
            *end_ptr = *prev;
        } else if (prev_valid && *prev > 0) { // coalesce only left side
            int new_block_size = *prev + *it + sizeof(int) * 2;
            int* end_ptr = reinterpret_cast<int*>(reinterpret_cast<char*>(&(*prev)) + new_block_size + sizeof(int));
            *prev = new_block_size;
            *end_ptr = *prev;
        } else if (next_valid && *next > 0) { // coalesce only right side
            int new_block_size = *it + *next + sizeof(int) * 2;
            int* end_ptr = reinterpret_cast<int*>(reinterpret_cast<char*>(&(*it)) + new_block_size + sizeof(int));
            *it = new_block_size;
            *end_ptr = *it;
        }
    }

public:
    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        if (N < sizeof(T) + sizeof(int) * 2)  {
            std::bad_alloc e;
            throw e;
        }
        int free_block_size = N - sizeof(int) * 2;
        (*this)[0] = free_block_size;
        (*this)[N - sizeof(int)] = free_block_size;
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type n) {
        if (n == 0) {
            throw std::bad_alloc();
        }
        pointer address = nullptr;
        int to_allocate = n * sizeof(T);
        iterator b = begin();
        while (b != end() && address == nullptr) {
            int block_size = *b;
            if (block_size >= to_allocate) {
                char* start_ptr = reinterpret_cast<char*>(&(*b));
                address = reinterpret_cast<pointer>(start_ptr + sizeof(int));
                int min_remainder = sizeof(T) + sizeof(int) * 2;
                if (block_size - to_allocate < min_remainder) {
                    // allocate entire block
                    int* end_ptr = reinterpret_cast<int*>(start_ptr + block_size + sizeof(int));
                    *b = -1 * block_size;
                    *end_ptr = *b;
                } else {
                    // allocate busy block
                    int* busy_end_ptr = reinterpret_cast<int*>(start_ptr + to_allocate + sizeof(int));
                    *b = -1 * to_allocate;
                    *busy_end_ptr = *b;
                    // change free block size
                    ++b;
                    int left_over = block_size - (to_allocate + sizeof(int) * 2);
                    int* free_end_ptr = reinterpret_cast<int*>(reinterpret_cast<char*>(&(*b)) + left_over + sizeof(int));
                    *b = left_over;
                    *free_end_ptr = *b;
                }
            }
            ++b;
        }
        assert(valid());
        if (address == nullptr) {
            std::bad_alloc e;
            throw e;
        }
        return address;
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());                            // from the prohibition of new
    }

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * flips the sign of the block pointed to by p, then calls coalesce()
     */
    void deallocate (pointer p, size_type) {
        if (p < reinterpret_cast<pointer>(&((*this)[0])) || p >= reinterpret_cast<pointer>(&((*this)[N]))) {
            throw std::invalid_argument("p is invalid");
        }
        int* start_ptr = reinterpret_cast<int*>(reinterpret_cast<char*>(p) - sizeof(int));
        int block_size = *start_ptr;
        if (block_size > 0) {
            throw std::invalid_argument("p points to a free block");
        }
        int* end_ptr = reinterpret_cast<int*>(reinterpret_cast<char*>(p) + abs(block_size));
        *start_ptr *= -1;
        *end_ptr = *start_ptr;
        coalesce(start_ptr);
        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(&(*this)[0]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return const_iterator(&(*this)[0]);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        return iterator(&(*this)[N]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        return const_iterator(&(*this)[N]);
    }
};

#endif // Allocator_h
