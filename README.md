# CS371p: Object-Oriented Programming Allocator Repo

* Name: Bren Keeton

* EID: tck548

* GitLab ID: tkeeton

* HackerRank ID: keeton

* Git SHA: 4654564c91d5f937bb3cd8ddcc72b071e72b2439

* GitLab Pipelines: https://gitlab.com/tkeeton/cs371p-allocator/-/pipelines

* Estimated completion time: 8 hours

* Actual completion time: 10 hours

* Comments:
